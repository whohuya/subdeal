import React, { Component } from "react";
import { connect } from "dva";
import { Link } from "dva/router";
import { Checkbox, Input, Button, Alert, Icon, Form } from "antd";
import styles from "./Login.less";

const FormItem = Form.Item;

@connect(({ login, loading }) => ({
  login,
  submitting: loading.effects["login/login"]
}))
class NormalLoginForm extends Component {
  state = {
    type: "account",
    autoLogin: true
  };

  onTabChange = type => {
    this.setState({ type });
  };

  handleSubmit = (e, values) => {
    console.log(values);
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(values);
        this.props.dispatch({
          type: "auth/signUp",
          // type: 'login/login',
          payload: values
        });
      }
    });
  };

  renderMessage = content => {
    return (
      <Alert
        style={{ marginBottom: 24 }}
        message={content}
        type="error"
        showIcon
      />
    );
  };

  render() {
    const { submitting } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className={styles.main}>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <FormItem>
            {getFieldDecorator("username", {
              rules: [
                { required: true, message: "Please input your username!" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="用户名"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("password", {
              rules: [
                { required: true, message: "Please input your Password!" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="密码"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("email", {
              rules: [
                { required: true, message: "Please input your email!" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="email"
                placeholder="邮箱"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("name", {
              rules: [
                { required: true, message: "Please input your Name!" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="name"
                placeholder="用户名"
              />
            )}
          </FormItem>
          <FormItem>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              登 录
            </Button>
            <span style={{ marginLeft: 16 }}>仅限管理员登录</span>
          </FormItem>
        </Form>
      </div>
    );
  }
}

const LoginPage = Form.create()(NormalLoginForm);

export default LoginPage;
