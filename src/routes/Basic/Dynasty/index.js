/**
 * Created by zhaoyu on Jan 16, 2018.
 */
import React, { PureComponent } from 'react'
import { connect } from 'dva'

import {
  Card,
  Spin,
  Popconfirm,
  Table,
  Button,
  Icon,
  Dropdown,
  Menu,
  Input,
  Modal,
  message
} from 'antd'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout'

import DynastyForm from './Form'
import TableCountText from '../../../components/TableCountText'
import Separator16 from '../../../components/Separator16'

const Column = Table.Column
@connect(state => ({
  loading: state['basic/dynasty'].loading,
  list: state['basic/dynasty'].data.list,
  pagination: state['basic/dynasty'].data.pagination
}))
export default class Dynasty extends PureComponent {
  state = {
    dynasty: null,
    sorter: null,
    filters: null,
    filterVisible: false,
    filtered: false,
    searchText: '',
    modalTitle: '',
    modalContent: null,
    modalFooter: null,
    submitButtonText: '',
    modalVisible: false,
    submitting: false
  }

  componentDidMount () {
    console.warn(this.props.state)
    this.fetchDynasty()
  }

  fetchDynasty = (payload = null) => {
    const {dispatch, pagination} = this.props
    const {sorter, filters, searchText} = this.state
    console.warn('in fetchDynasty ')
    if (!payload) {
      dispatch({
        type: 'basic/dynasty/fetch',
        payload: {
          search: searchText,
          page: pagination ? pagination.current : 1,
          showSizeChanger: pagination ? pagination.showSizeChanger : false,
          sorter,
          ...filters
        }
      })
    } else {
      dispatch({type: 'basic/dynasty/fetch', payload})
    }
    console.warn('fetchDynasty Ends')
  }

  onInputChange = (e) => {
    this.setState({searchText: e.target.value})
  }

  onDynastySearch = async () => {
    const {pagination} = this.props
    const {sorter, filters, searchText} = this.state
    this.fetchDynasty({
      search: searchText,
      page: 1,
      pageSize: pagination.pageSize,
      showSizeChanger: pagination.showSizeChanger,
      sorter,
      ...filters
    })
    // set data
    this.setState({
      filterVisible: false,
      filtered: !!searchText
    })
  }

  handleTableChange = async (pagination, filters, sorter) => {
    const {searchText} = this.state
    // const sorter = (sorter.order === 'ascend') ? `${sorter.field}` : `-${sorter.field}`
    console.warn('pagination: ', pagination)
    this.setState({
      sorter,
      filters
    })
    this.fetchDynasty({
      search: searchText,
      page: pagination.current,
      pageSize: pagination.pageSize,
      showSizeChanger: pagination.showSizeChanger,
      sorter,
      ...filters
    })
  }

  editDynasty = async (dynasty: ?Object = null) => {
    const {submitting} = this.state
    const [title, buttonText] = (dynasty === null) ? [
      '创建朝代', '创建'
    ] : [
      '编辑朝代', '编辑'
    ]
    await this.setState({
      dynasty: (dynasty === null) ? {
        id: null
      } : dynasty
    })
    await this.setState({
      modalTitle: title,
      modalContent: <DynastyForm
        dynasty={dynasty}
        title={title}
        submitButtonText={buttonText}
        onSubmit={this.onSubmit}
        submitting={submitting} />,
      submitButtonText: buttonText,
      modalVisible: true
    })
  }

  deleteDynasty = async (dynasty) => {
    const {dispatch} = this.props
    dispatch({
      type: 'basic/dynasty/remove',
      payload: {dynasty},
      callback: this.deleteDynastyCallback
    })
  }

  deleteDynastyCallback = () => {
    message.success(`成功删除该学科。`)
    this.fetchDynasty()
  }

  handleCancel = async () => {
    await this.setState({
      modalVisible: false,
      modalContent: null,
      dynasty: null
    })
  }

  onSubmit = (values) => {
    const {dispatch} = this.props
    const {dynasty} = this.state
    console.warn(values)
    console.warn(dynasty)
    try {
      this.setState({submitting: true})
      const newDynasty = {
        ...values,
        serial: values['serial'] ? parseFloat(values['serial']) : 0
      }
      if (!!dynasty && !!dynasty['id']) {
        console.warn(dynasty)
        // update a dynasty
        dispatch({
          type: 'basic/dynasty/update',
          payload: {dynasty: newDynasty},
          callback: () => this.fetchDynasty()
        })
      } else {
        // add a new dynasty
        dispatch({
          type: 'basic/dynasty/add',
          payload: {dynasty: newDynasty},
          callback: () => this.fetchDynasty()
        })
      }
    } catch (error) {
      console.log(error)
    } finally {
      this.setState({submitting: false})
    }
    // finish submit
    this.setState({
      modalVisible: false,
      modalContent: null,
      dynasty: null
    })
  }

  render () {
    const {list, pagination, loading} = this.props
    const {filtered, filterVisible, modalTitle, modalVisible, modalContent, modalFooter} = this.state
    return (
      <PageHeaderLayout title='朝代列表'>
        <Card bordered={false}>
          {pagination && <TableCountText>{pagination.total}</TableCountText>}
          <Modal title={modalTitle}
            visible={modalVisible}
            onCancel={this.handleCancel}
            footer={modalFooter}>
            {modalContent || <Spin spinning={loading} />}
          </Modal>
          <Button type='primary'
            onClick={() => this.editDynasty()}>
            新建朝代
          </Button>
          <Separator16 />
          <Table dataSource={list && list.map(o => ({id: o.id, ...o.attributes}))}
            loading={loading}
            rowKey='id'
            onChange={this.handleTableChange}
            pagination={pagination}
          >
            <Column
              title='编号'
              sorter
              dataIndex='serial'
            />
            <Column
              title='名称'
              dataIndex='name'
              key='name'
              filterDropdown={<div className='table-input-search'>
                <Input
                  placeholder='名称'
                  value={this.state.searchText}
                  onChange={this.onInputChange}
                  onPressEnter={this.onDynastySearch}
                />
                <Button type='primary' onClick={this.onDynastySearch}>检索</Button>
              </div>}
              filterIcon={<Icon type='search'
                style={{color: filtered ? '#108ee9' : '#aaa'}} />}
              filterDropdownVisible={filterVisible}
              onFilterDropdownVisibleChange={(visible) => {
                this.setState({
                  filterVisible: visible
                })
              }}
            />
            <Column
              title='操作'
              render={(text, dynasty) => (
                <div>
                  <a onClick={() => this.editDynasty(dynasty)}>编辑</a>
                  <span className='ant-divider' />
                  <Dropdown overlay={
                    <Menu>
                      <Menu.Item>
                        <Popconfirm title='确定要删除吗？'
                          onConfirm={() => this.deleteDynasty(dynasty)}>
                          <a href='#'>删除</a>
                        </Popconfirm>
                      </Menu.Item>
                    </Menu>
                  }>
                    <a className='ant-dropdown-link'>
                      更多 <Icon type='down' />
                    </a>
                  </Dropdown>
                </div>
              )}
            />
          </Table>
        </Card>
      </PageHeaderLayout>
    )
  }
}
