/**
 * Created by zhaoyu on Jan 16, 2018.
 * @flow
 */
import React from 'react'
import { connect } from 'dva'
import { Field, reduxForm } from 'redux-form'

import { Button, Form } from 'antd'

import {
  TextField, NumberField
} from '../../../common/form/Fields'
import { required } from '../../../helpers/formHelper'

type Props = {
  dynasty: ?Object,
  handleSubmit: () => void,
  pristine: boolean,
  submitting: boolean,
  submitButtonText: string,
  reset: () => void,
  onChange: () => void
}

type State = {
  loading: boolean
}

class DynastyForm extends React.Component<void, Props, State> {
  state = {
    loading: false
  }

  render () {
    const {handleSubmit, submitButtonText, submitting} = this.props
    return (
      <div>
        <Form onSubmit={handleSubmit} layout='horizontal' className='modal-form'>
          <Field name='serial' type='text' size='default'
            placeholder='序号' label='序号'
            required hasFeedback validate={[required]}
            labelCol={{'span': 6}} wrapperCol={{'span': 18, 'offset': 0}}
            min={1} max={99}
            component={NumberField}
          />
          <Field name='name' type='text' size='default'
            placeholder='请输入朝代名称' label='朝代名称'
            required hasFeedback validate={[required]}
            labelCol={{'span': 6}} wrapperCol={{'span': 18}}
            component={TextField}
          />
          <Button
            type='primary' htmlType='submit' disabled={submitting}
            className='modal-form-button'>{submitButtonText}</Button>
        </Form>
      </div>
    )
  }
}

const ExportDynastyForm = reduxForm({
  form: 'dynastyForm'
})(DynastyForm)

function mapStateToProps (state, ownProps) {
  const {dynasty} = ownProps
  return {
    initialValues: dynasty,
    submitButtonText: ownProps.submitButtonText || '提交'
  }
}

export default connect(mapStateToProps)(ExportDynastyForm)
